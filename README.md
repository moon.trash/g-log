# g-log
g-log (for git log), is just a bunch of scripts to allow you to micro-blog using git commits.

## How it works
When you create a commit, the pre-commit hook runs a perl script to export a git log in a neat and simple HTML file.

## Requirements
- Linux/UNIX system
- Perl

## Installation
Just copy the hook in your .git/hooks directory.